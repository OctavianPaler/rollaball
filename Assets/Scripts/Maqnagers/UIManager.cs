﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager> {

   [SerializeField]
   private Text _scoreText;

   [SerializeField]
   private Text _gameOverText;

   public Text GameOverText => _gameOverText;

   public Text ScoreText => _scoreText;

   private new void Awake()
   {
      base.Awake();

      if(null == _scoreText)
      {
         throw new ArgumentNullException(nameof(_scoreText));
      }
   }

   private new void OnDestroy()
   {
      base.OnDestroy();
   }
}
