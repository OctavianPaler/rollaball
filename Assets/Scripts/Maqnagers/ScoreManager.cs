﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : Singleton<ScoreManager>
{
   [SerializeField] private GameObject _pickUpParent;

   private int _scoreToReach;
   public int Score { get; set; }

   private new void Awake()
   {
      _scoreToReach = _pickUpParent.transform.childCount;

      
      ResetScore();
      base.Awake();
   }

   private new void OnDestroy()
   {
      base.OnDestroy();
   }

   public void IncrementScore()
   {
      Score++;
      UIManager.Instance.ScoreText.text = $"Score: {Score}";

      if (Score == _scoreToReach)
      {
         UIManager.Instance.GameOverText.text = "You won!";
         GameManager.Instance.GameState = GameState.FINISHED;
      }
   }

   public void ResetScore()
   {
      Score = 0;
      UIManager.Instance.ScoreText.text = $"Score: {Score}";
   }
}
