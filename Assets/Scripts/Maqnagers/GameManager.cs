﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{

   private GameState _gameState;

   public GameState GameState
   {
      get { return _gameState; }
      set { _gameState = value; }
   }

   protected override void Awake()
   {
      base.Awake();
      _gameState = GameState.STANDBY;
   }

   protected override void OnDestroy()
   {
      base.OnDestroy();
   }

   public void LoadScene(string sceneName)
   {
      SceneManager.LoadSceneAsync("SampleScene");
      _gameState = GameState.PLAYING;
   }

   public void Update()
   {
      if (Input.GetKeyDown(KeyCode.R))
      { 
         SceneManager.LoadSceneAsync("SampleScene");
         _gameState = GameState.PLAYING;
         Destroy(ScoreManager.Instance);
      }

   }
}
