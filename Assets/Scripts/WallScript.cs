﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallScript : MonoBehaviour {

   private void OnTriggerEnter(Collider other)
   {
      if (other.gameObject.tag.Equals("Player"))
      {
         UIManager.Instance.GameOverText.text = "You lose!";
         Destroy(other.gameObject);
      }
   }
}
