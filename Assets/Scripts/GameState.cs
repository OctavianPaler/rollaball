﻿public enum GameState
{
   DEFAULT,
   STANDBY,
   PLAYING,
   FINISHED
}