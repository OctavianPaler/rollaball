﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

   private Rigidbody rigidBody;

   [SerializeField]
   private float _speed;

   private void Start()
   {
      rigidBody = GetComponent<Rigidbody>();
   }

   private void Update()
   {
      if (GameManager.Instance.GameState == GameState.FINISHED)
      {
         rigidBody.velocity = Vector3.zero;
         return;
      }

      float horizontalMovement = Input.GetAxis("Horizontal");
      float verticalMovement = Input.GetAxis("Vertical");

      //Debug.Log(Time.deltaTime);

      rigidBody.AddForce(new Vector3(horizontalMovement, 0f, verticalMovement)* _speed * Time.deltaTime);
   }

}
