﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{

   private Vector3 _offset;

   [SerializeField]
   private Transform _player;

   // Use this for initialization
   void Start()
   {
      if (null == _player)
      {
         throw new ArgumentNullException(nameof(_player));
      }
      _offset = transform.position - _player.position;
   }

   // Update is called once per frame
   void LateUpdate()
   {
      if (null == _player)
         return;

      transform.position = _player.position + _offset;
   }
}
